# think-auth

#### 介绍
thinkphp权限管理类

#### 软件架构
软件架构说明


#### 安装教程

打开命令面板，进入到项目根目录
~~~
composer require tensent/think-auth
~~~

#### 使用说明

~~~
$Auth = new \sent\auth\Auth();

$rule   当前要检验的规则
$uid    当前登录用户
$type   规则类型
$mode   验证模式
$Auth->check($rule, $uid, $type, $mode)
~~~
